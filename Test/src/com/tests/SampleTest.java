/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.tests;


import org.testng.annotations.Test;

import com.components.entities.Start;
import com.iwaf.framework.Initiator;

public class SampleTest extends Initiator{


//----------------------------------------------------------Hippo - India--------------------------------------------------------------------------
	
	@Test(description = "VerifyHomePageLayout", testName = "HippoIndia -1", groups = "HippoIndia")
	public void VerifyHomePageLayout() {
		Start.asTester.goToHippoHomePage_India().VerifyHomePageLayout();
	}

	@Test(description = "VerifyNoResultPage", testName = "HippoIndia -19", groups = "HippoIndia")
	public void VerifyNoResultPage() {		
		Start.asTester.goToHippoHomePage_India().NoResultPage("SearchData3");
	}

	@Test(description = "VerifyPDPDisplayPage", testName = "HippoIndia -11,20", groups = "HippoIndia")
	public void VerifyPDPDisplayPage() {
		Start.asTester.goToHippoHomePage_India().VerifyPDPDisplay("SearchData4");
	}

	@Test(description = "VerifyQuickview", testName = "HippoIndia -10", groups = "HippoIndia")
	public void VerifyQuickview() {
		Start.asTester.goToHippoHomePage_India().VerifyQuickview();

	}

	@Test(description = "UberCategoryPage", testName = "HippoIndia -6", groups = "HippoIndia")
	public void UberCategoryPage() {
		Start.asTester.goToHippoHomePage_India().UberCategoryPage();
	}

	@Test(description = "VerifyCategoryPageTemplates", testName = "HippoIndia-7", groups = "HippoIndia")
	public void VerifyCategoryPageTemplates() {

		Start.asTester.goToHippoHomePage_India().VerifyCategoryPageTemplates();
	}

	@Test(description = "VerifyPriceUpdate", testName = "HippoIndia-14", groups = "HippoIndia")
	public void VerifyPriceUpdate() {
		Start.asTester.goToHippoHomePage_India().VerifyPriceUpdate("SearchData5");
	}

	@Test(description = "VerifyStoreLocatorDisplay", testName = "HippoIndia -17", groups = "HippoIndia")
	public void VerifyStoreLocatorDisplay() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyStoreLocatorDisplay();
	}

	@Test(description = "VerifyFooterLayout", testName = "HippoIndia-5", groups = "HippoIndia")
	public void VerifyFooterLayout() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyFooterLayout();
	}

	@Test(description = "VerifyCareandCleaningPage", testName = "HippoIndia-23", groups = "HippoIndia")
	public void VerifyCareandCleaningPage() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyCareandCleaningPage();
	}

	@Test(description = "VerifySorting", testName = "HippoIndia-9", groups = "HippoIndia")
	public void VerifySorting() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifySorting();
	}
	
	@Test(description = "Verify Global Navigation unfolding navigation", testName = "HippoIndia- 4", groups = "HippoIndia")
	public void verifyglobalNavUnfolding() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_GlobalNavUnfolding();
	}

     @Test(description = "Verify place an enquiry page", testName = "HippoIndia - 12", groups = "HippoIndia")
	public void verifyplaceandenquiry() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_PlaceanEnquiryCTA();
	}

	@Test(description = "Verify literature page", testName = "HippoIndia - 22", groups = "HippoIndia")
	public void verifyliteraturepage() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_LiteraturePage();
	}

	@Test(description = "Verify correct display of contact us page", testName = "HippoIndia - 21", groups = "HippoIndia")
	public void verifycontactuspage() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_ContactUsPage();
	}

	@Test(description = "Verify Hero Image", testName = "HippoIndia - 2", groups = "HippoIndia")
	public void verifyHeroImage() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_HeroImage();
	}

	@Test(description = "Verify World Wide Banner", testName = "HippoIndia-3", groups = "HippoIndia")
	public void verifyWorldWideBanner_hippoIndia() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_WorldWideBanner();
	}

	@Test(description = "Verify Compare Feature", testName = "HippoIndia - 15,16", groups = "HippoIndia")
	public void verifycomparefeature() throws InterruptedException {
		Start.asTester.goToHippoHomePage_India().VerifyHippoIndia_Comparefeature();
	}
	
//----------------------------------------------------------Hippo - Africa-------------------------------------------------------------------------
	
	@Test(groups="HippoAfrica",description = " Verify general layout of Africa Home Page" ,testName ="Hippo_Africa: 1")
	public void VerifyHomePageLayout_HippoAfrica()
	{	
		Start.asTester.goToHippoAfricapage().VerifyHomePageLayout();
	}

	@Test(groups="HippoAfrica",description = "Verify Hero Carousel", testName = "Hippo_Africa: 2-Part1")
	public void VerifyHerocarousel()
	{
	    Start.asTester.goToHippoAfricapage().HippoAfrica_HeroImageCarousel();  
	}
	
	@Test(groups="HippoAfrica",description = "Verify Hero Carousel", testName = "Hippo_Africa: 2-Part2")
	public void VerifyHeroImagenavigation()
	{
	    Start.asTester.goToHippoAfricapage().VerifyHeroImageNavigation();
	}
	
	@Test(groups="HippoAfrica",description = "Verify display of Where to Buy & our brands", testName = "Hippo_Africa: 3,5")
	public void VerifyUtilitybar() throws InterruptedException
	{
	    Start.asTester.goToHippoAfricapage().VerifyUtilityBar();
	}
	
	@Test(groups="HippoAfrica",description = "Verify Footer and access of each link under Footer", testName = "Hippo_Africa: 4")
	public void VerifyFooter_HippoAfrica()
	{
	    Start.asTester.goToHippoAfricapage().VerifyFooter();       
	}

	@Test(groups="HippoAfrica",description = " Verify Contact Us page can be accessed via footer,global nav" ,testName ="Hippo_Africa: 6")	
	public void VerifyAccessContactUsPage()
	{
	    Start.asTester.goToHippoAfricapage().VerifyAccessContactUsPage().VerifyContactUsAlignment();
	}

	@Test(groups="HippoAfrica",description = "Verify Literature page" ,testName ="Hippo_Africa: 8")	
	public void VerifyLiteraturePage()
	{
	    Start.asTester.goToHippoAfricapage().LiteraturePage();
	}
	
	@Test(groups="HippoAfrica",description = "Verify Highlights page" ,testName ="Hippo_Africa: 9")	
	public void VerifyHighlightsPage()
	{
	    Start.asTester.goToHippoAfricapage().HighlightsPage();
	}

	
//------------------------------------------------------------------------Hippo - Brazil----------------------------------------------------------
	
	
	@Test(description = "Verifying general layout of homepage", testName = "Hippo Brazil-1", groups = "HippoBrazil")
	public void VerifyHomePageLayout_HippoBrazil() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_HomePageLayout();		
	}
	
	@Test(description = "Verifying hero image/carousel", testName = "Hippo Brazil-2: Part1", groups = "HippoBrazil")
	public void VerifyHeroImage_Carousel() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_HeroImageCrousel();		
	}
	
	@Test(description = "Verifying hero image Navigation", testName = "Hippo Brazil-2: Part2", groups = "HippoBrazil")
	public void VerifyHeroImage_Navigation() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_HeroImageNavigation();	
	}
	
	@Test(description = "Verifying worldwide banner", testName = "Hippo Brazil Utilitybar-3", groups = "HippoBrazil")
	public void VerifyworldwideBanner() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().VerifyHippoBrazil_WorldWide();		
	}
	
	@Test(description = "Verifying global nav unfolding animation", testName = "Hippo Brazil-4", groups = "HippoBrazil")
	public void VerifyGlobalNavunfolding() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_GlobalNavunfolding();	
	}
	
	@Test(description = "Verifying footer layout", testName = "Hippo Brazil-5", groups = "HippoBrazil")
	public void Verifyfooterlayout() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_Footer();		
	}
	
	@Test(description = "Verifying category page templates", testName = "Hippo Brazil-6", groups = "HippoBrazil")
	public void VerifyCategoryPageTemplate() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_CategoryPageTemlate();		
	}

	@Test(description = "Verifying filters work properly", testName = "Hippo Brazil-7", groups = "HippoBrazil")
	public void VerifyFilter() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_Filter();	
	}
	
	@Test(description = "Verifying sorting works properly", testName = "Hippo Brazil-8", groups = "HippoBrazil")
	public void VerifySort() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_Sort();	
	}
	
	@Test(description = "Verifying Quickview works properly", testName = "Hippo Brazil-9", groups = "HippoBrazil")
	public void VerifyQuickView() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_QuickView();	
	}
	
	@Test(description = "Verifying PDP template works properly", testName = "Hippo Brazil-10", groups = "HippoBrazil")
	public void VerifyPDPTemplate_HippoBrazil() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().VerifyPDPTemplatePage("SearchDataBrazil");	
	}
	
	@Test(description = "Verifying SKU update", testName = "Hippo Brazil-11", groups = "HippoBrazil")
	public void Verify_SkuUpdate() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_SkuUpdate("SearchDataBrazil2");	
	}
	
	@Test(description = "Verifying Compare feature", testName = "Hippo Brazil Compare-13,14", groups = "HippoBrazil")
	public void VerifyCompareFeature() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().VerifyCompareFeature();	
	}
	
	@Test(description = "Verifying no results page", testName = "Hippo Brazil-15", groups = "HippoBrazil")
	public void Verify_SearchResultsPage() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_SearchResultsPage("SearchDataBrazil3");	
	}
	
	@Test(description = "Verifying no results page", testName = "Hippo Brazil-16", groups = "HippoBrazil")
	public void VerifyPDP_NoResults() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_PDP_NoResults("SearchDataBrazil1");	
	}

	@Test(description = "Verifying PDP displays when SKU# is entered", testName = "Hippo Brazil-17", groups = "HippoBrazil")
	public void VerifyPDP_Sku() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_PDPDisplayPage("SearchDataBrazil");		
	}
	
	@Test(description = "Verifying Care And Cleaning page", testName = "Hippo Brazil-18", groups = "HippoBrazil")
	public void VerifyCareAndCleaningpage() throws InterruptedException
	{
		Start.asTester.goToHippo_Brazil().HippoBrazil_FooterCareandCleaningpage();	
	}
	
	
//----------------------------------------------------------Hippo Latamx-------------------------------------------------
	
	@Test(groups="HippoLatamx", description = "Verify CAD symbols page", testName= "Latamx Page -26")
	public void verifyCADSymbolsPage()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyCADSymbolsPage();
	}
	
	@Test(groups="HippoLatamx", description = "Verify Contact Us page " , testName= "Latamx Page -24")
	public void verifyContactUs()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyContactUsPage();
	}

	@Test(groups="HippoLatamx", description = "verify Discontinued SKU In LA " , testName= "Latamx Page -23")
	public void VerifyDiscontinuedSkuLAAndMexico()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyDiscontinuedSKUInLA("SearchDataLA").verifyDiscontinuedSKUInMexico("SearchForMexico");
	}

	@Test(groups="HippoLatamx", description = "verify Literature From Footer and Bathroom" , testName= "Latamx Page -25")
	public void VerifyLiteratureFromBathroomAndFooter()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyLiteratureFromFooter().verifyLiteratureFromBathroom();
	}

	@Test(groups="HippoLatamx", description = "verify Where To Buy link" , testName= "Latamx Page -17")
	public void VerifyWheretoBuy()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyWhereToBuy();
	}

	@Test(groups="HippoLatamx", description = "verify Footer Layout" , testName= "Latamx Page -6")
	public void VerifyFooter()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyFooterLayout();
	}

	@Test(groups="HippoLatamx", description = "verify World wide Banner", testName= "Latamx Page -3")
	public void verifyWorldWideBanner()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyKohlerWorldWide();
	}
	
	@Test(groups="HippoLatamx", description = "verify TypeAhead Spanish and English", testName= "Latamx Page -19")
	public void VerifyTypeAheadSpanishAndEN()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyTypeAheadSpanish("SearchSuggestion").verifyTypeAheadEnglish("SearchSuggestionEN");
	}
	
	@Test(groups="HippoLatamx", description = "verify PDP Template", testName= "Latamx Page -12")
	public void VerifyPDPTemplate_HippoLatamx()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyPDPPage("SearchPDP");
	}
  
	@Test(groups="HippoLatamx", description = "verify Uber Category page", testName= "Latamx Page -7")
    public void VerifyUberCategory()
    {
		Start.asTester.goToHippo_LatinAmerica().verifyUberCategoryPage();	  
    }
	
    @Test(groups="HippoLatamx", description = "verify Hero Carousel", testName= "Latamx Page -2")
	public void VerifHeroCarousel()
	{
	   Start.asTester.goToHippo_LatinAmerica().verifyHeroImageCrousel().verifyHeroImageNavigation();
	}
  
    @Test(groups="HippoLatamx", description = "verify Compare Feature", testName= "Latamx Page-15,16 ")
    public void VerifyCompareModal()
    {
	   Start.asTester.goToHippo_LatinAmerica().VerifyComparefeature();
    }
    
    @Test(groups="HippoLatamx", description = "Verify HomePage layout", testName= "Hippo2->Latamx: 1")
	public void hippoLatinAmerica_HomePageLayout()
	{
	  Start.asTester.goToHippo_LatinAmerica().verify_HomePageLayout();
	}
	
	@Test(groups="HippoLatamx", description = "Verify No results page", testName= "Hippo2->Latamx: 21")
	public void verifyNoResultsPage()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyNoResultsPage("SearchData");
	}
	
	@Test(groups="HippoLatamx", description = "Verify Product Description Page", testName= "Hippo2->Latamx: 22")
	public void verifyPDPDisplays()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyNoResultsPage("SearchDataPDP");
	}
	
	@Test(groups="HippoLatamx", description = "Verify language selector", testName= "Hippo2->Latamx: 4")
	public void verifylanguageSelector()
	{
		Start.asTester.goToHippo_LatinAmerica().verifylanguageSelector();
	}
	
	@Test(groups="HippoLatamx", description = "Verify global nav unfolding animation", testName= "Hippo2->Latamx: 5")
	public void verifyUnfoldingAnimation()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyUnfoldingAnimation();
	}
	
	@Test(groups="HippoLatamx", description = "Verify Quickview works properly", testName= "Hippo2->Latamx: 11")
	public void verifyQuickViewOfAProduct()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyQuickViewOfAProduct();
	}
	
	@Test(groups="HippoLatamx", description = "Verify search results page", testName= "Hippo2->Latamx: 20")
	public void verifySearchResultsPage()
	{
		Start.asTester.goToHippo_LatinAmerica().verifySearchResultsPage("SearchWord");
	}
	
	@Test(groups="HippoLatamx", description = "Verify category productGrid", testName= "Hippo2->Latamx: 8")
	public void verifyCategoryPage()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyCategoryPage();
	}
	
	@Test(groups="HippoLatamx", description = "Verify different color chips and sku update", testName= "Hippo2->Latamx: 13")
	public void verifySKUUpdate()
	{
		Start.asTester.goToHippo_LatinAmerica().verifySKUUpdate("SearchSKU");
	}
	
	@Test(groups="HippoLatamx", description = "Verify product category sorting filter", testName= "Hippo2->Latamx: 10")
	public void verifyProductCategorySortingFilter()
	{
		Start.asTester.goToHippo_LatinAmerica().verifyProductCategorySortingFilter();
	}

	
//-------------------------------------------------------------Hippo1 test cases------------------------------------------------------------

	/* Industrial Test Cases*/
	@Test(groups={"IndustrialPage"}, description = "Verify Industrial Home Page Layout", testName = "Hippo->Industrial:1")
	public void verifyIndustrialHomePageLayout() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.VerifyHomePage();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify hero image",testName="Hippo->Industrial:2")
	public void VerifyIndustrialHeroImage() throws InterruptedException
	{
		Start.asTester
		.goToIndustrialPage()
		.VerifyHippoIndustrial_HeroImage();
	} 
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industrial Worldwide functionality", testName="Hippo->Industrial:3")
	public void VerifyIndustrialWWFunctionality() throws InterruptedException 
	{			
	
		Start.asTester
		.goToIndustrialPage()
		.Industrial_WorldWide();	
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Footer", testName = "Hippo->Industrial:4")
	public void verifyIndustryFooter() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.VerifyFooter();
	}
	 
	@Test(groups={"IndustrialPage"}, description = "Verify category page templates",testName="Hippo->Industrial:5")
	public void VerifycategoryforIndustrial() throws InterruptedException
	{
		Start.asTester
		.goToIndustrialPage()
		.VerifyCategorypageforIndustrial();
	} 

	@Test(groups={"IndustrialPage"}, description = "Verify filters for KW range",testName="Hippo->Industrial:6")
	public void Verifyslidersforindustrial() throws InterruptedException
	{
		Start.asTester
		.goToIndustrialPage()
		.VerifyProductfilters();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify PDP Page", testName = "Hippo->Industrial:8")
	public void verifyIndustryPDPPage() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyPDP();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify compare feature",testName="Hippo->Industrial:10,11")
    public void Verifycomparefeature() throws InterruptedException
    {
		Start.asTester
		.goToIndustrialPage()
		.VerifyHippoIndustrial_Comparefeature();
    }
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Distributor Locator", testName = "Hippo->Industrial:12")
	public void verifyIndustryDistributorLocator() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyDistributorLocator();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Search Page", testName = "Hippo->Industrial:13")
	public void verifyIndustrySearchPage() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifySearchPage();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Search no Results", testName = "Hippo->Industrial:14")
	public void verifyIndustryNoResults() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyNoResults();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Search Product", testName = "Hippo->Industrial:15")
	public void verifyIndustrySearchProduct() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyProductResults();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Contact Us", testName = "Hippo->Industrial:16")
	public void verifyIndustryContactUsPage() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyContactUs();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Article Page", testName = "Hippo->Industrial:18")
	public void verifyIndustryArticlePage() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyArticle();
	}
	
	@Test(groups={"IndustrialPage"}, description = "Verify Industry Literature Page", testName = "Hippo->Industrial:19")
	public void verifyIndustryLiteraturePage() 
	{	
		Start.asTester
		.goToIndustrialPage()
		.verifyLiterature();
	}
	
	/* Engines Test Cases*/
	@Test(groups={"EnginesPage"}, description = "Verify Engines HomePage Layout", testName = "Hippo->Engines:1")
	public void verifyEnginesHomePageLayout() 
	{	
		Start.asTester
		.goToEnginesPage()
		.VerifyHomePage();
	}
	
    @Test(groups={"EnginesPage"}, description = "Verify hero image",testName="Hippo->Engines:2")
    public void VerifyheroimageforEngines() throws InterruptedException
    {
    	Start.asTester
    	.goToEnginesPage()
    	.VerifyHippoEngine_HeroImage();
    } 
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Worldwide functionality", testName="Hippo->Engines:3")
	public void VerifyEnginesWWFunctionality() throws InterruptedException
	{			
		Start.asTester
		.goToEnginesPage()
		.Engines_WorldWide();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Footer", testName = "Hippo->Engines:4")
	public void verifyEnginesFooter() 
	{	
		Start.asTester
		.goToEnginesPage()
		.VerifyFooter();
	}
	
    @Test(groups={"EnginesPage"}, description = "Verify category page templates",testName="Hippo->Engines:5")
    public void VerifycategoryforEngines() throws InterruptedException
    {
    	Start.asTester
    	.goToEnginesPage()
    	.VerifyCategorypageforEngines();
    } 
	
    @Test(groups={"EnginesPage"}, description = "Verify sliders for KW range",testName="Hippo->Engines:6")
    public void VerifyslidersforEngines() throws InterruptedException
    {
           Start.asTester
           .goToEnginesPage()
           .VerifyProductSliderforEngines();
    } 

	@Test(groups={"EnginesPage"}, description = "Verify Engines PDP Page", testName = "Hippo->Engines:8")
	public void verifyEnginesPDPPage() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifyPDP();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Compare functionality", testName="Hippo->Engines:10,11")
	public void VerifyEnginesCompare() throws InterruptedException
	{
		Start.asTester					
		.goToEnginesPage()			
		.VerifyCompareFeature();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Find A Dealer", testName = "Hippo->Engines:12")
	public void verifyEnginesFindADealer() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifyFindDealer();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Search Page", testName = "Hippo->Engines:13")
	public void verifyEnginesSearchPage() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifySearchPage();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Search no Results", testName = "Hippo->Engines:14")
	public void verifyEnginesNoResults() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifyNoResults();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Search Product", testName = "Hippo->Engines:15")
	public void verifyEnginesSearchProduct() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifyProductResults();
	}
	
	@Test(groups={"EnginesPage"}, description = "Verify Engines Contact Us", testName = "Hippo->Engines:16")
	public void verifyEnginesContactUs() 
	{	
		Start.asTester
		.goToEnginesPage()
		.verifyContactUs();
	}
	
	/* Portable Test Cases*/
	@Test(groups={"PortablePage"}, description = "Verify Portable HomePage Layout", testName = "Hippo->Portable:1")
	public void verifyPortableHomePage() 
	{	
		Start.asTester
		.goToPortablePage()
		.VerifyHomePage();
	}
	
    @Test(groups={"PortablePage"}, description = "Verify hero image",testName="Hippo->Portable:2")
    public void Verifyheroimageforportable() throws InterruptedException
    {
    	Start.asTester
    	.goToPortablePage()
    	.VerifyHippoPortable_HeroImage();
    } 
	
	@Test(groups={"PortablePage"}, description = "Verify Portables Worldwide functionality", testName="Hippo->Portables:3")
	public void VerifyPortablesWWFunctionality() throws InterruptedException 
	{			
		Start.asTester
		.goToPortablePage()			
		.Portables_WorldWide();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable Footer", testName = "Hippo->Portable:4")
	public void verifyPortableFooter() 
	{	
		Start.asTester
		.goToPortablePage()
		.VerifyFooter();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify category page templates", testName= "Hippo1->portable:5")
	public void VerifyCategoryPageBreadcrumbs()
	{
		Start.asTester
		.goToPortablePage()
		.VerifyBreadcrumbs();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify filters work properly", testName= "Hippo1->portable:6")
	public void VerifySliderFilter()
	{
		Start.asTester
		.goToPortablePage()
		.verifyProductCategorySlider();
	}
		
	@Test(groups={"PortablePage"}, description = "Verify sorting works properly", testName= "Hippo1->portable:7")
	public void VerifySortingFilter()
	{
		Start.asTester
		.goToPortablePage()
		.verifyProductCategorySortingFilter();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify pagination and 'view' dropdown works properly", testName= "Hippo1->portable:8")
	public void VerifyPagination()
	{
		Start.asTester
		.goToPortablePage()
		.verifyProductCategoryPagination();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify product modules", testName= "Hippo1->portable:9")
	public void VerifyProductModules()
	{
		Start.asTester
		.goToPortablePage()
		.verifyProductCategoryProductModules();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable PDP Page", testName = "Hippo->Portable:10")
	public void verifyPortablePDPPage() 
	{	
		Start.asTester
		.goToPortablePage()
		.verifyPDP();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable Find A Dealer", testName = "Hippo->Portable:14")
	public void verifyPortableFindADealer() 
	{	
		Start.asTester
		.goToPortablePage()
		.verifyPortableFindDealer();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable Search Results (External Link)", testName = "Hippo->Portable:15")
	public void verifyPortableResults() 
	{	
		Start.asTester
		.goToPortablePage()
		.verifySearchResults();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable Search no Results", testName = "Hippo->Portable:16")
	public void verifyPortableNoResults() 
	{	
		Start.asTester
		.goToPortablePage()
		.verifyNoResults();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Portable Contact Us", testName = "Hippo->Portable:17")
	public void verifyPortableContactUs() 
	{	
		Start.asTester
		.goToPortablePage()
		.verifyContactUs();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Custom Kits page", testName= "Hippo1->portable:18")
	public void VerifyCustomKitPage()
	{
		Start.asTester
		.goToPortablePage()
		.VerifyCustomKits();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify Owner's manuals page", testName= "Hippo1->portable:19")
	public void VerifyOwnersManualPage()
	{
		Start.asTester
		.goToPortablePage()
		.VerifyOwnerManualsPage();
	}
	
	@Test(groups={"PortablePage"}, description = "Verify article page in Generators 101 page", testName= "Hippo1->portable:20")
	public void VerifyArticlePageGenerators101()
	{
		Start.asTester
		.goToPortablePage()
		.VerifyArticlePage();
	}
}

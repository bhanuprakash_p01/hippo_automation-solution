package com.components.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.openqa.selenium.Keys;

import com.components.repository.SiteRepository;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

public class Hippo_Africa extends SitePage{
	
	//Defining locators in Hippo Africa Page	
	public static final Target HeroPrevArrow  = new Target("HeroPrevArrow","//*[@id='myCarousel']/a/button[@class='slick-prev']",Target.XPATH);
	public static final Target HeroNextArrow  = new Target("HeroNextArrow","//*[@id='myCarousel']/a/button[@class='slick-next']",Target.XPATH);
	public static final Target AfricaMap  = new Target("AfricaMap","//*[@class='map']/img",Target.XPATH);
	public static final Target AfricaMap_Country  = new Target("AfricaMap_Country","//div[@id='Angola']/ol/li/p[2]",Target.XPATH);
	public static final Target AfricaMap_Country_Attribute  = new Target("AfricaMap_Country_Attribute","//*[@id='Angola']",Target.XPATH);
	public static final Target OurBrands  = new Target("AfricaMap","//*[@class='sub-top-nav']/li[@class='brand-link']",Target.XPATH);
	public static final Target WhereToBuy = new Target("WhereToBuy","//*[@class='sub-top-nav']/li[@class='locator-link']",Target.XPATH);
	public static final Target Hero = new Target("Hero","//*[@id='myCarousel']/div",Target.XPATH);
	public static final Target PromoModules =  new Target("PromoModules","//*[@class='row main-content']/div/div/div/div[2]/div",Target.XPATH);
	public static final Target Footer = new Target("Footer","//*[@id='footer']/div[1]/div",Target.XPATH);
	public static final Target Literature = new Target("Literature","//li[@class='nav-btns']/ul/li[2]/a",Target.XPATH);
	public static final Target Literature_Header = new Target("Literature_Header","//header[@class='page-header template-header']/h2",Target.XPATH);
	public static final Target Highlights = new Target("Highlights","//li[@class='nav-btns']/ul/li[4]/a",Target.XPATH);
	public static final Target Highlights_Header = new Target("Highlights_Header","//div[@class='hero-text']/div/h2",Target.XPATH);

	Actions Action = new Actions(getCommand().driver);

	JavascriptExecutor JS = (JavascriptExecutor)getCommand().driver;
    
    //For getting Browser Name
  	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	
    public Hippo_Africa(SiteRepository repository)
	{
		super(repository);
	}

    public Hippo_Africa _GoToHippoAfricaPage() 
	{	  
		log("Navigate to Hippo Africa Page",LogType.STEP);
		return this;	 
	}
    //Verify general layout of Hippo Africa Home Page
    public Hippo_Africa VerifyHomePageLayout()
  	{
    	String browserName = caps.getBrowserName();
  		try
  		{			
  			String OurBrandsText = getCommand().getText(OurBrands);
  			
  			OurBrandsText = OurBrandsText.substring(0,10);
  			
  			if(getCommand().isTargetVisible(OurBrands) && OurBrandsText.equals("Our Brands"))
  		    {
  	           	log(OurBrandsText+" is displayed",LogType.STEP);
  		    }
  		    else
  		    {
  		        log(OurBrandsText+" is not displayed",LogType.ERROR_MESSAGE);
  		        Assert.fail(OurBrandsText+"is not displayed");
  		    }			
  			
  			String WhereToBuyText = getCommand().getText(WhereToBuy);
  			
  			if(browserName.equals("MicrosoftEdge"))
			{
  				WhereToBuyText = WhereToBuyText.substring(0,12);
			}
  			
  			if(getCommand().isTargetVisible(WhereToBuy) && WhereToBuyText.equals("Where to Buy"))
  		    {
  	           	log(WhereToBuyText+" is displayed",LogType.STEP);
  		    }
  			else
  		    {
  		        log(WhereToBuyText+" is not displayed",LogType.ERROR_MESSAGE);
  		        Assert.fail(WhereToBuyText+"is not displayed");
  		    }
  			  				
  			String HeroText = getCommand().getAttribute(Hero, "class");
  	
  			if(getCommand().isTargetVisible(Hero) && HeroText.contains("hero"))
  			{
  			       log("Hero is displayed",LogType.STEP);
  			}
  			else
  			{
  			       log("Hero is not displayed",LogType.ERROR_MESSAGE);
  			       Assert.fail("Hero is not displayed");			     
  			} 						
  	
  			String PromoModulesText = getCommand().getAttribute(PromoModules, "class");
  	
  			if(getCommand().isTargetVisible(PromoModules) && PromoModulesText.contains("row promotions"))
  			{
  			       log("PromoModules is displayed",LogType.STEP);
  			}
  	
  			else
  			{
  			       log("PromoModules is not displayed",LogType.ERROR_MESSAGE);
  			       Assert.fail("PromoModules is not displayed");
  			}
  	
  			String FooterText = getCommand().getAttribute(Footer, "class");
  	
  			if(getCommand().isTargetVisible(Footer) && FooterText.contains("foot"))
  			{
  			       log("Footer is displayed",LogType.STEP);
  			}
  	
  			else
  			{
  			       log("Footer is not displayed",LogType.ERROR_MESSAGE);
  			       Assert.fail("Footer is not shown");
  			}
			
			List<String> GlobalNav = new ArrayList<String>();
			
			GlobalNav.add("/html/body/div[1]/div[1]/div/ul/li[3]/ul/li[1]/a");
			GlobalNav.add("/html/body/div[1]/div[1]/div/ul/li[3]/ul/li[2]/a");
			GlobalNav.add("/html/body/div[1]/div[1]/div/ul/li[3]/ul/li[3]/a/img");
			GlobalNav.add("/html/body/div[1]/div[1]/div/ul/li[3]/ul/li[4]/a");
			GlobalNav.add("/html/body/div[1]/div[1]/div/ul/li[3]/ul/li[5]/a");
			
			for(String Xpath: GlobalNav)
			{
				WebElement Element = getCommand().driver.findElement(By.xpath(Xpath));
				String NavText = Element.getText();
				if(Element.isDisplayed())
				{
					if(!NavText.isEmpty())
					{
						if(NavText.equals("ABOUT US") || NavText.equals("LITERATURE") || NavText.equals("HIGHLIGHTS") || NavText.equals("CONTACT US")) 
						{
							log(NavText+" is displayed in Global Navigation", LogType.STEP);
							log("Correct Text: "+NavText+" is displayed in Global Navigation",LogType.STEP);
						}
						else
						{
							log("Correct Text: "+NavText+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
							Assert.fail("Correct Text: "+NavText+" is not displayed in Global Navigation");
						}
					}
					else
					{
						log("Logo is displayed in Global Navigation",LogType.STEP);
					}
				}
				else
				{
					 if(!NavText.isEmpty())
					 {
		                    log(NavText+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
		                    Assert.fail(NavText+" is not displayed in Global Navigation");
					 }
		             else
		             {
		                    log("Logo is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
					        Assert.fail("Logo is not displayed in Global Navigation");
		             }
				}			
			}	
	}
  	
  catch(Exception ex)
  {
  	Assert.fail(ex.getMessage());
  }
  return this;
}  
    
	//Verify other Kohler brands and Where to Buy display
	public Hippo_Africa VerifyUtilityBar() throws InterruptedException
	{
		String browserName = caps.getBrowserName();
		try
		{
			List<WebElement> UtilityBar = getCommand().driver.findElements(By.xpath("//*[@class='container']/div/div/ul/li[1]/ul[1]/li"));
			int LinksCount = UtilityBar.size();
			int ExpectedLiksCount = 2;
			log("Verifying utility bar",LogType.STEP);

			if(LinksCount == ExpectedLiksCount)
			{
				log("Utility bar contains "+LinksCount+ " links",LogType.STEP);
				log("Verifying each link",LogType.STEP);
				for(WebElement Utilitybar:UtilityBar)
				{
					String UtilityBarText = Utilitybar.getText();
					UtilityBarText = UtilityBarText.replaceAll(">", "");
					UtilityBarText = UtilityBarText.replaceAll(" ", "");
					if(UtilityBarText.equals("OurBrands")) 
					{
						log(UtilityBarText+" is present in utility bar",LogType.STEP);
						log("Expanding "+UtilityBarText,LogType.STEP);
						Utilitybar.click();
						
						log("Verifying navigation of each brand present under "+ UtilityBarText,LogType.STEP);
						List<WebElement> Brands = getCommand().driver.findElements(By.xpath("//*[@class='container']/div/div/ul/li[2]/ul/li/a"));	
						for(WebElement Brand:Brands)
						{
							String href = Brand.getAttribute("href");
							getCommand().waitFor(2);
							log("Clicking on the brand with link: "+href,LogType.STEP);
							String SelectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
							Brand.sendKeys(SelectLinkOpeninNewTab);
							getCommand().waitFor(7);
							ArrayList<String> ListOfTabs = new ArrayList<String>(getCommand().driver.getWindowHandles());
							getCommand().driver.switchTo().window(ListOfTabs.get(1));
							getCommand().waitFor(3);
							String CurrentPageTitle = getCommand().driver.getTitle();
							log("Navigated to brand '"+CurrentPageTitle+"'",LogType.STEP);
							String[] Parts = CurrentPageTitle.split("\\|");
							String Part;
							for(int i=0;i<Parts.length;i++)
							{
								Part = Parts[i].replace(" ", "").trim().toLowerCase();
								
								if(href.contains(Part) || Part.contains("destinationkohler"))
								{
									log("Clicking on brand "+Part+" navigating to correct page",LogType.STEP);
									break;
								}
							}
							getCommand().driver.close();
				    		getCommand().driver.switchTo().window(ListOfTabs.get(0));				
						}
					}
					
					if(UtilityBarText.equals("WheretoBuy"))
					{						
						log(UtilityBarText+" is present in utility bar",LogType.STEP);
						log("Clicking on  "+UtilityBarText,LogType.STEP);
						Utilitybar.click();
						getCommand().waitFor(3);
						log("Checking Africa MAP is present in Where to buy page",LogType.STEP);
						getCommand().waitForTargetVisible(AfricaMap, 120);
						if(getCommand().isTargetPresent(AfricaMap))
						{
							String MapText = getCommand().getAttribute(AfricaMap, "alt");
							MapText = MapText.substring(0, MapText.length() - 4);
							log("Where to Buy page displays with an "+MapText,LogType.STEP);
							log("Getting all the countries displayed in the MAP and checking they are highlighted",LogType.STEP);
							List<WebElement> AfricaCountries = getCommand().driver.findElements(By.xpath("//*[@class='stores-map']/div[2]/div/div"));	
							for(WebElement AfricaCountrie:AfricaCountries)
							{
								String color = AfricaCountrie.getCssValue("color"); 
								String backcolor = AfricaCountrie.getCssValue("background-color");
								String id = AfricaCountrie.getAttribute("id");
								id = id.substring(4);
								if(color!=backcolor)
								{
									log("Country "+id.toUpperCase()+" is diplayed and it is hihglighted in the map",LogType.STEP);
								}
								
								else
								{
									log("Country "+id.toUpperCase()+" is diplayed and it is not hihglighted in the map",LogType.ERROR_MESSAGE);
									Assert.fail("Country "+id.toUpperCase()+" is diplayed and it is not hihglighted in the map");
								}
								
							}
							
							log("Click on any country and verify corresponding information displays on a left section",LogType.STEP);
							
							WebElement Country = AfricaCountries.get(0);
							if(browserName.equals("internet explorer")) {
								JS.executeScript("window.scrollBy(0,200)");
							}
							
							Country.click();
							getCommand().waitFor(3);
							String id = AfricaCountries.get(0).getAttribute("id");
							id = id.substring(4);					
							getCommand().waitForTargetVisible(AfricaMap_Country, 120);
							String DisplayedCountry = getCommand().getText(AfricaMap_Country);
							getCommand().waitForTargetVisible(AfricaMap_Country_Attribute, 120);
							String Content = getCommand().getAttribute(AfricaMap_Country_Attribute, "class");				
							if(Content.contains("active") && DisplayedCountry.toUpperCase().equals(id.toUpperCase()))
							{
								log("After clicking on country "+id+" corresponding information displays on a left section",LogType.STEP);								
							}
							
							else
							{
								log("After clicking on country "+id+" corresponding information is not displays on a left section",LogType.ERROR_MESSAGE);
								Assert.fail("After clicking on country "+id+" corresponding information is not displays on a left section");
							}						
						}
						
					}
					
				}
			}
			
			else
			{
				log("Mismatch in expected links to be present in utility bar | Expected: "+ ExpectedLiksCount+ ", Actual: "+LinksCount,LogType.STEP);
				Assert.fail("Mismatch in expected links to be present in utility bar");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Footer layout
	public Hippo_Africa VerifyFooter()
	{
		String browserName = caps.getBrowserName();
		log("Verifying footer is composed of 5 columns Links",LogType.STEP);
        // get the list of links available in footer
        List<WebElement> footer = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div[@class='col-1-of-5-md col-full-sm']"));
        int  footerlinks_count = footer.size();
        if(footerlinks_count == 5)
        {
        	log("Footer is composed of 5 columns Links",LogType.STEP);
        	List<WebElement> footerHeaders = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div[@class='col-1-of-5-md col-full-sm']/button"));
        	for(WebElement footerHeader:footerHeaders) 
        	{
        		String HeaderText = footerHeader.getText();
        		log("'"+HeaderText+"' column is displayed under Footer section",LogType.STEP);
        	}
        }
        
        else
        {
        	log("Footer is not composed of 5 columns Links",LogType.ERROR_MESSAGE);
        	Assert.fail("Footer is not composed of 5 columns Links");
        }     
        try 
        {
        	String PageUrl= getCommand().getPageUrl();
        	log("Checking the navigation of each link under Footer section",LogType.STEP);
        	for (int j = 1; j <= footerlinks_count; j++) 
            {
                  List<WebElement> footerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div["+j+"]/div/a"));
                  int footerheaderlinksCount = footerheaderlinks.size();
                  for(int k=0; k<footerheaderlinksCount;k++)
                  {
                	  List<WebElement> footerheaderLinks = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div["+j+"]/div/a"));
                	  String Linktext = footerheaderLinks.get(k).getText();
                	  
                	  log("Clicking on the link "+Linktext,LogType.STEP);
                	  getCommand().waitFor(1);
                      JS.executeScript("arguments[0].scrollIntoView(true);", footerheaderLinks.get(k));
                	  String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
                	  footerheaderLinks.get(k).sendKeys(selectLinkOpeninNewTab);
                      getCommand().waitFor(7);
                      ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                      if(listofTabs.size()>1)
      	              {
                    	  log("Switching to new tab",LogType.STEP);
                          getCommand().driver.switchTo().window(listofTabs.get(1));
                          getCommand().waitFor(3);
                          log("Getting new tab page title",LogType.STEP);
          	    		  String Currentpageurl = getCommand().getPageUrl();
          	    		  
          	    		  if(PageUrl.equals(Currentpageurl))
          	    		  {	    			
          	    			log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
          	    			Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
          	    		  }
          	    		  else
          	    		  {    			
          	    			log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
          	    		  }
          	    		
          	    		  getCommand().driver.close();
          	    		  getCommand().driver.switchTo().window(listofTabs.get(0));

      	              }
                      
                      else
                      {
                    	  if(browserName.equals("internet explorer")) {
      		            	getCommand().waitFor(2);
      					}
                    	  String Currentpageurl = getCommand().getPageUrl();
                    	  if(PageUrl.equals(Currentpageurl))
          	    		  {	    			
          	    			log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
          	    			Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
          	    		  }
          	    		  else
          	    		  {    			
          	    			log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
          	    		  }
                    	  
                    	  getCommand().goBack();
                    	  getCommand().waitFor(3);
                      }
                  }  	                  
            }
        	
        }catch(Exception ex)
        {
               ex.getMessage();
        }

		return this;
	}
    
	//Verify that Contact Us page can be accessed via Global nav
	public Hippo_Africa VerifyAccessContactUsPage()
	{
		try {
		   
			getCommand().driver.findElement(By.partialLinkText("CONTACT")).click();
			getCommand().waitFor(5);
			String Title1=getCommand().driver.getTitle();
			getCommand().driver.navigate().back();
			getCommand().driver.findElement(By.partialLinkText("Contact")).click();
			getCommand().waitFor(5);
			String Title2=getCommand().driver.getTitle();
			Assert.assertEquals(Title1, Title2,"Mismatch in Page titles for Contact us page");
			log("Contact Us page can be accessed via Global Nav and Footer",LogType.STEP);
	     }		
		catch(Exception ex)
		{
			Assert.fail("Contact Us page cannot be accessed via GLobal Nav and footer");
	    }
        return this;
	}      
    
	//Verify form fields are aligned to the right, and a left section with extra information.
    public Hippo_Africa VerifyContactUsAlignment()
     {
     	try {	
    			Point Informtion = getCommand().driver.findElement(By.xpath("//*[@class='col-md-3 col-sm-3 col-xs-12']")).getLocation();
    		    int xcord_Informtion = Informtion.getX();
    			 
    			 Point Form = getCommand().driver.findElement(By.xpath("//*[@id='contact-us']")).getLocation();
    			 int xcord_Form = Form.getX();
    			 
    			 if(xcord_Informtion<xcord_Form)
    			 {
    				 log("Information column is on the left", LogType.STEP);
    			 }
    			 
    			 else
    			 {
    				 log("Form fields is not on the left", LogType.ERROR_MESSAGE);
    				 Assert.fail("Form fields column is not on the left");
    			 } 				
    	     }		
    		catch(Exception ex)
    		{
    			 Assert.fail("Form fields column is not on the left");
    	    }
      return this;
    	}

    //Verify Global nav links
	public Hippo_Africa VerifyGlobalNavigation()
	{
		try
		{
			List<WebElement> GlobalNavigations = getCommand().driver.findElements(By.xpath("//*[@class='nav-btns']/ul/li/a"));
			
			for(WebElement GlobalNavigation : GlobalNavigations)
			{
				String GlobalNavText = GlobalNavigation.getText();
				
				switch(GlobalNavText)
				{    
				   case "ABOUT US":  
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break; 
				   case "LITERATURE":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break;
				   case "HIGHLIGHTS":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break;
				   case "CONTACT US":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break; 
				    
				   default:
					   log("Logo is present at Global Navigation",LogType.STEP);
						String pageTitle = getCommand().driver.getTitle();
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
						GlobalNavigation.sendKeys(selectLinkOpeninNewTab);
				        getCommand().waitFor(10);
				        ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				        log("Switching to new tab",LogType.STEP);
				        getCommand().driver.switchTo().window(listofTabs.get(1));
		                String CurrentpageTitle = getCommand().driver.getTitle();
		                
		                if(CurrentpageTitle.equals(pageTitle))
		                {
		                	log("Clicking on Logo is redirecting to same current page",LogType.STEP);
		                }                
		                else
		                {
		                	log("Clicking on Logo is not redirecting to same current page",LogType.ERROR_MESSAGE);
		                	Assert.fail("Clicking on Logo is not redirecting to same current page");
		                }
		                getCommand().driver.close();
		          		getCommand().driver.switchTo().window(listofTabs.get(0));
				}
			}
		}
		
		catch(Exception ex) 
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Verify Literature page
	public Hippo_Africa LiteraturePage()
	{	
		try
		{
			log("Clicking on Literature Page",LogType.STEP);
			getCommand().isTargetPresent(Literature);
			getCommand().click(Literature);
			
			getCommand().waitForTargetVisible(Literature_Header, 120);

	        List<WebElement> Modules = getCommand().driver.findElements(By.xpath("//*[@class='feature-section']/div/div/figure"));
	        int ModulesCount =  Modules.size();
	        log(ModulesCount+ " modules present in a literature page",LogType.STEP);
	        log("Checking each modules in a literature page has a PDF link",LogType.STEP);
	        int PDFCounter = 0;
	        for(int i=1; i<=ModulesCount;i++)
	        {
	        	JS.executeScript("window.scrollBy(0,50)");
	        	
	        	getCommand().waitFor(3);

	        	String Text = getCommand().driver.findElement(By.xpath("//*[@class='feature-section']/div/div/figure["+i+"]/article/img")).getAttribute("title");
	        	
	        	WebElement PDF = getCommand().driver.findElement(By.xpath("//*[@class='feature-section']/div/div/figure["+i+"]/article/figcaption/footer/a"));
	        	
	        	String Href = PDF.getAttribute("href");
	        	
	        	log("Checking module "+Text+" in a literature page has a PDF link",LogType.STEP);
	        	
	        	if(PDF.isDisplayed() && Href.endsWith(".pdf"))
	        	{
	        		log("For module "+Text+" PDF link is present",LogType.STEP);
	        		PDFCounter++;      		
	        	}
	        	
	        	else
	        	{
	        		log("For module "+Text+" no PDF link is present",LogType.ERROR_MESSAGE);
	        		Assert.fail("For module "+Text+" no PDF link is present");
	        	}
	        	
	        }
	        
	        if(PDFCounter == ModulesCount)
	        {
	        	log("Each module has a PDF link",LogType.STEP);     	
	        }
	        
	        else
	        {
	        	log("Some module doesn't has PDF link",LogType.ERROR_MESSAGE);  
	        	Assert.fail("Some module doesn't has PDF link");
	        }
		}
		catch(Exception ex) 
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}	
	
	//Verify Highlights page
	public Hippo_Africa HighlightsPage()
	{	
		String browserName = caps.getBrowserName();
		try
		{
			log("Clicking on Highlights Page",LogType.STEP);
			
			getCommand().isTargetPresent(Highlights);
			getCommand().click(Highlights);			
			getCommand().waitForTargetVisible(Highlights_Header, 120);
			String Pageurl= getCommand().getPageUrl();			
	        log("Getting Total no. of modules present in a page",LogType.STEP);
	        List<WebElement> ModulesList = getCommand().driver.findElements(By.xpath("//*[@class = 'feature-box-title']/a"));
	        int ModulesCount =  ModulesList.size();
	        log(ModulesCount+ " modules present in a Highlights page",LogType.STEP);
	        log("Checking each modules in a Highlights page is a link to an article page",LogType.STEP);
	        int ArticleCounter = 0;
	        for(int i=0; i<ModulesCount;i++)
	        {
	        	List<WebElement> Modules = getCommand().driver.findElements(By.xpath("//*[@class = 'feature-box-title']/a"));
	        	String Href = Modules.get(i).getAttribute("href");	        	
	        	getCommand().waitFor(2);
	        	String selectModuleOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	        	Modules.get(i).sendKeys(selectModuleOpeninNewTab);
	            getCommand().waitFor(5);
	            
	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	            if(listofTabs.size()>1)
	            {
	            	log("Switching to new tab",LogType.STEP);
		            getCommand().driver.switchTo().window(listofTabs.get(1));
		            if(browserName.equals("internet explorer")) {
		            	getCommand().waitFor(2);
					}
		            
		            String Currentpageurl = getCommand().getPageUrl();
		            if(Pageurl.equals(Currentpageurl))
			    	{	    			
			    		log("Clicking on link "+Href+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			    		Assert.fail("Clicking on link "+Href+" is not redirecting to the corresponding page");
			    	}
			    	else
			    	{    			
			    		log("Clicking on link "+Href+" is redirecting to the corresponding page",LogType.STEP);
			    		ArticleCounter++;
			    	}
		            
		        	getCommand().driver.close();

	                getCommand().driver.switchTo().window(listofTabs.get(0));
	            }
	            
	            else
	            {
	            	String Currentpageurl = getCommand().getPageUrl();
		            if(Pageurl.equals(Currentpageurl))
			    	{	    			
			    		log("Clicking on link "+Href+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			    		Assert.fail("Clicking on link "+Href+" is not redirecting to the corresponding page");
			    	}
			    	else
			    	{    			
			    		log("Clicking on link "+Href+" is redirecting to the corresponding page",LogType.STEP);
			    		ArticleCounter++;
			    	}
		            
		            getCommand().goBack();
		            getCommand().waitFor(3);
	            }
	            
	        }
	        
	        if(ArticleCounter == ModulesCount)
	        {
	        	log("Each module is a link to an article page",LogType.STEP);     	
	        }
	        
	        else
	        {
	        	log("Each module is not a link to an article page",LogType.ERROR_MESSAGE);    
	        	Assert.fail("Not article links");
	        }
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
       
    //Verify that clicking on Hero image redirects to correspondent page
    public Hippo_Africa VerifyHeroImageNavigation()
	{
    	try
		{	List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='myCarousel']/ol/li"));
			
			int i=1;
			
			log("Clicking on each nav dots and clicking on image displayed for each nav dot",LogType.STEP);
			
			for(int k=0;k<NavDots.size();k++)
			{
				List<WebElement> Navdots = getCommand().driver.findElements(By.xpath("//*[@id='myCarousel']/ol/li"));
				
				String PageUrl = getCommand().getPageUrl();
				
				log("Clicking on "+i+" Nav dot",LogType.STEP);
				
				Navdots.get(k).click();
				
				getCommand().waitFor(3);
				
                WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+i+"]"));
				
				if(Image.isDisplayed())
				{
					String ImageText = Image.getText();	
					
					log("Clicking on image "+ ImageText,LogType.STEP);
					
					Image.click();
					
					getCommand().waitFor(4);
					
					String CurrentPageUrl = getCommand().getPageUrl();
					
					Assert.assertNotEquals(PageUrl, CurrentPageUrl,"Clicking on image "+ ImageText+ " is not redirecting to its site");
					
					log("Clicking on image "+ ImageText+ " is redirecting to its site: "+getCommand().getPageTitle(),LogType.STEP);
					
					getCommand().goBack();
					
					getCommand().waitFor(3);
								
					i++;				
				}
				
				else
				{
					log("Failed to click on "+i+" Image",LogType.ERROR_MESSAGE);
					Assert.fail("Failed to click on "+i+" Image");
				}
			}  
			}	
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
	
		return this;
	}
    
    //Verify Hero carousel using NavDots,Arrows
    public Hippo_Africa HippoAfrica_HeroImageCarousel()
    {
    	try
		{
			log("Getting Total no. of nav dots available in hero grid",LogType.STEP);
			List<WebElement> Navdots = getCommand().driver.findElements(By.xpath("//*[@id='myCarousel']/ol/li"));
			
			int j=1;
			int n=5;
			
			log("Total No. of nav dots available in hero grid are: "+Navdots.size(),LogType.STEP);
			ArrayList<String> list = new ArrayList<String>();
			List<String> Imagetext_NextArrow = new ArrayList<String>();
			List<String> Imagetext_PrevArrow = new ArrayList<String>();
			
			log("Clicking on each nav dots available in hero grid and checking image changes",LogType.STEP);
			for(int i = 0; i < Navdots.size(); i++)
			{			
				Navdots.get(i).click();
				getCommand().waitFor(3);
				String NavDotClass = Navdots.get(i).getAttribute("class");
				int index=i+1;
				if(NavDotClass.contains("active"))
				{					
					String NavDotXpath = "//*[@id='myCarousel']/div/div/div/div["+index+"]/div/h1";
					String ImageText = getCommand().getDriver().findElement(By.xpath(NavDotXpath)).getText();
					log(index+" nav dot is active and moving to next nav dots",LogType.STEP);
					if(ImageText.equals("")) 
					{
						String NavDotXpath_Emptytext = "//*[@id='myCarousel']/div/div/div/div["+index+"]/div/h1[2]";
						String ImageText_Empty = getCommand().getDriver().findElement(By.xpath(NavDotXpath_Emptytext)).getText();
						list.add(ImageText_Empty);		
					}					
					else
					{
						list.add(ImageText);		
					}
					
				}
				else
				{
					log(index+" Nav dot is not acive after clicking on it",LogType.ERROR_MESSAGE);    
		        	Assert.fail(index+" Nav dot is not acive after clicking on it");
				}
			}
			
            Assert.assertTrue(CompareDataFromSameList(list),"Image not changes for every nav dot click");
			
			log("Image changes for every nav dot click",LogType.STEP);
			
			Navdots.get(4).click();
			
			log("Checking Scroll feature using next arrow",LogType.STEP);
			
			for(int k=1;k<=Navdots.size();k++)
			{
				log("Clicking on next arrow and checking image changes",LogType.STEP);
				
				getCommand().mouseHover(HeroNextArrow).click(HeroNextArrow);
				
				getCommand().waitFor(2);
				
				WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+k+"]/a/img"));
				
				if(Image.isDisplayed())
				{
					String NavDotXpath = "//*[@id='myCarousel']/div/div/div/div["+j+"]/div/h1";
					String ImageText = getCommand().getDriver().findElement(By.xpath(NavDotXpath)).getText();
					log(j+" nav dot is active and moving to next nav dots",LogType.STEP);
					if(ImageText.equals("")) 
					{
						String NavDotXpath_Emptytext = "//*[@id='myCarousel']/div/div/div/div["+j+"]/div/h1[2]";
						String ImageText_Empty = getCommand().getDriver().findElement(By.xpath(NavDotXpath_Emptytext)).getText();
						Imagetext_NextArrow.add(ImageText_Empty);		
					}					
					else
					{
						Imagetext_NextArrow.add(ImageText);		
					}
					j++;
				}

				else
				{
					log("Image is not displayed after clicking on next arrow",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on next arrow");
				}
			}
			
            log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			Assert.assertTrue(CompareDataFromSameList(Imagetext_NextArrow),"Clicking on next arrow, image not changes and scroll is not working");
			
			log("Clicking on next arrow, image changes and scroll is working",LogType.STEP);
			
			getCommand().mouseHover(HeroNextArrow).click(HeroNextArrow);
			
			log("Checking Scroll feature using previous arrow",LogType.STEP);

			for(int m=1;m<=Navdots.size();m++)
			{
				log("Clicking on prev arrow and checking image changes",LogType.STEP);
				
				getCommand().mouseHover(HeroPrevArrow).click(HeroPrevArrow);
				
				getCommand().waitFor(2);
				
                WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+n+"]/a/img"));
				
				if(Image.isDisplayed())
				{
					String NavDotXpath = "//*[@id='myCarousel']/div/div/div/div["+n+"]/div/h1";
					String ImageText = getCommand().getDriver().findElement(By.xpath(NavDotXpath)).getText();
					log(j+" nav dot is active and moving to next nav dots",LogType.STEP);
					if(ImageText.equals("")) 
					{
						String NavDotXpath_Emptytext = "//*[@id='myCarousel']/div/div/div/div["+n+"]/div/h1[2]";
						String ImageText_Empty = getCommand().getDriver().findElement(By.xpath(NavDotXpath_Emptytext)).getText();
						Imagetext_PrevArrow.add(ImageText_Empty);		
					}					
					else
					{
						Imagetext_PrevArrow.add(ImageText);		
					}
					n--;
				}

				else
				{
					log("Image is not displayed after clicking on next arrow",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on next arrow");
				}
			}
			
            log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			Assert.assertTrue(CompareDataFromSameList(Imagetext_PrevArrow),"Clicking on prev arrow, image not changes and scroll is not working");
			
			log("Clicking on prev arrow, image changes and scroll is working",LogType.STEP);

		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
    }
   
    //Method to verify data from same list to check Hero images navigation
	public boolean CompareDataFromSameList(List<String> list)
	{
		boolean status=true;
		label:
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					status=false;
					break label;
				}				      
			}	      
		}		
		return status;
	}
}  

  
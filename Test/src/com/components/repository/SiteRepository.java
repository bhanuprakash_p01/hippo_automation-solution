/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.repository;

import com.components.pages.Hippo_Africa;
import com.components.pages.Hippo_Brazil;
import com.components.pages.Hippo_EnginesPage;
import com.components.pages.Hippo_India;
import com.components.pages.Hippo_IndustrialPage;
import com.components.pages.Hippo_LatinAmerica;
import com.components.pages.Hippo_PortablePage;



public class SiteRepository{

    
    public Hippo_Brazil hippo_Brazil()
	
	{
		return new Hippo_Brazil(this);
	}

    public Hippo_LatinAmerica hippo_LatinAmerica()
	
	{
		return new Hippo_LatinAmerica(this);
	}
    
    public Hippo_Africa hippoAfricaPage()
	{
		return new Hippo_Africa(this);
	}
    
    public Hippo_India HippoHomePage_India() {
    	
    	return new Hippo_India(this);
    }
    
    public Hippo_IndustrialPage industrialPage()
	{
		return new Hippo_IndustrialPage(this);
	}
	
	public Hippo_EnginesPage enginesPage()
	{
		return new Hippo_EnginesPage(this);
	}
	
	public Hippo_PortablePage portablePage()
	{
		return new Hippo_PortablePage(this);
	}

		
}
package com.components.yaml;

import com.iwaf.framework.BasePage;

public class EnginesData 
{
	public String search_NoResult;
	public String search_Result;
	public String search_Product;
	public String search_Header;
	public String page_Title;
	public String dealer_Header;
	public String deal_ZipCode;
	public String deal_ZipCode1;
	public String contact_Header;
	public String contact_us_firstname;
	public String contact_us_lastname;
	public String contact_us_email;
	public String contact_us_phone;
	public String contact_us_address;
	public String contact_us_address2;
	public String contact_us_city;
	public String contact_us_postalcode;
	public String contact_us_company;
	public String contact_us_model;
	public String contact_us_comments;
	public String notalink;
	public String notalink1;
	public String banner_text;
	public String contact_Page_Title;
	public String pdp_Tab1;
	public String pdp_Tab2;
	public String pdp_Tab3;
	
	public static EnginesData fetch(String key)
	{
		BasePage pageObj = new BasePage();
		EnginesData obj = pageObj.getCommand().loadYaml(key, "data-pool/Engines_Data.yaml");
		return obj;
	}
}

/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.entities;

import com.components.pages.Hippo_Africa;
import com.components.pages.Hippo_Brazil;
import com.components.pages.Hippo_EnginesPage;
import com.components.pages.Hippo_India;
import com.components.pages.Hippo_IndustrialPage;
import com.components.pages.Hippo_LatinAmerica;
import com.components.pages.Hippo_PortablePage;
import com.components.repository.SiteRepository;

public class Tester {
	SiteRepository repository =new SiteRepository();
	
	public Hippo_Brazil goToHippo_Brazil(){	
		return this.repository.hippo_Brazil().atHippo_Brazil();
	}
	
	public Hippo_LatinAmerica goToHippo_LatinAmerica(){	
		return this.repository.hippo_LatinAmerica().atHippo_LatinAmerica();
	}
	
	public Hippo_Africa goToHippoAfricapage() {
		return this.repository.hippoAfricaPage()._GoToHippoAfricaPage();
	}
	
    public Hippo_India goToHippoHomePage_India() {		
		return this.repository.HippoHomePage_India().atHippoHomePage_India();
	}
    
    public Hippo_IndustrialPage goToIndustrialPage()
	{
		return this.repository.industrialPage().atIndustrialPage();
	}
	
	public Hippo_EnginesPage goToEnginesPage()
	{
		return this.repository.enginesPage().atEnginesPage();
	}
	
	public Hippo_PortablePage goToPortablePage()
	{
		return this.repository.portablePage().atPortablePage();
	}
	
}
